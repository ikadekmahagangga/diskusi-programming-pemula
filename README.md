# Diskusi Programming Pemula

Repo untuk group WA Diskusi Programming Pemula


# Jadwal Belajar Online


**Sabtu Pekan I**



- {sesi jam 20:00 | Javascript | meet | bang Tomo}
- {sesi jam 21:00 | }
- {sesi jam 22:00 | }



**Minggu Pekan I**



- {sesi jam 20:00 | Java | meet | Fikri}
- {sesi jam 21:00 | Nodejs Series | meet | Asrul}
- {sesi jam 22:00 |  }



**Sabtu Pekan II**



- {sesi jam 20:00 | PHP | meet | bang Tomo}
- {sesi jam 21:00 | HTML/CSS | meet | dhe studio}
- {sesi jam 22:00 | C# | meet | Diando}



**Minggu Pekan II**



- {sesi jam 20:00 | Linux | meet | Fikri}
- {sesi jam 21:00 |  }
- {sesi jam 22:00 | C++ | meet | Diando}



**Sabtu Pekan III**



- {sesi jam 20:00 | English for Programmer | meet | dhe studio}
- {sesi jam 21:00 | Nodejs Series | meet | Asrul}
- {sesi jam 22:00 |  }



**Minggu Pekan III**



- {sesi jam 20:00 | Laravel | meet | Yazid}
- {sesi jam 21:00 | }
- {sesi jam 22:00 | python | meet | Diando }




**Sabtu Pekan IV**



- {sesi jam 20:00 | Database | meet | bang Tomo}
- {sesi jam 21:00 | Typescript | meet | Asrul}
- {sesi jam 22:00 |  }



**Minggu Pekan IV**



- {sesi jam 20:00 | Git | meet | Fikri}
- {sesi jam 21:00 | Codeigniter | meet | dhe studio}
- {sesi jam 22:00 |  }




# Rencana Topik Belajar Online




JAVA



**Basic**



- konsep pemrograman
- tipe data
- CLI input/ouput
- procedure/method
- flow control
- looping
- rekursi




**Object Oriented Programming**



- Class & Object
- Enkapsulasi
- Inheritance
- Polimorphism




JAVASCRIPT




- variable
- array
- conditional
- loop
- function
- object
- json




HTML/CSS




- versi html 
- semantic web
- ruleset
- flex
- responsive





PHP





- variable 
- array
- conditional
- looping
- function
- mvc
- oop




CI 




- konfigurasi awal
- crud study kasus gaji pegawai




DATABASE



- pengenalan erd dan dfd
- ddl dan dml (dasar)




VERSION CONTROL




**GIT**



- Instalasi
- Inisialisasi git ke project
- Commit
- Branch
- Stash
- Git server
- Push & pull
- Kontribusi




LINUX



- Rufus
- instalasi VBOX
- partisi (EFI/Legacy)
- APT







Cara menggunakan repo gitlab





1. Silakan membuat **folder baru** untuk bahasa pemrograman yang script nya pengin di-share
2. Berikan keterangan jelas di commit script isinya tentang apa dan dari siapa. Misalnya **regexp untuk validasi email dari Bang Tomo**
3. Jika ada kesulitan silakan diskusikan dengan Bang Tomo
4. Untuk mengelola repo lewat android bisa dengan menginstall Termux for android trs ketikkan perintah berikut ini




  **apt update && apt upgrade**



  **apt install git**
  
  
  
  **termux-setup-storage**
  
  
  
  **cd shared** buat folder di sini
  
  
  
  **git clone https://gitlab.com/hibiscusht/diskusi-programming-pemula.git**